﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour
{
    public GameObject roomLight;
    public GameObject player;

    public bool lightOn;
    public Room[] refer;


    public void UpdateLigth(bool _value)
    {
        if (!player.activeSelf)
            return;
            
        lightOn = _value;
        roomLight.SetActive(lightOn);
    }

    public void UpdatePlayerPosition(bool _value)
    {
        player.SetActive(_value);
    }

    public bool Verify(string _room)
    {
        bool _succes = false;

        foreach (Room i in refer)
        {
            i.UpdatePlayerPosition(false);
            //Debug.Log(i.name + ":" + _room);
            if (i.name == _room && !i.lightOn && !_succes)
                _succes = true;
        }

        return _succes;
    }

}
