﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    bool moved;
    public int ghostPower;
    int roomID;
    public int moveCount;
    bool priest;
    int turnCount;

    public Room roomCokn;

    public bool changeLight;


    // Start is called before the first frame update
    void Start()
    {
        primary = roomCokn;
        ghostPower = 0;
        priest = false;
        moveCount = 1;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void EndTurnEvents()
    {
        GetPoint();
        PlayAudioClip();
        ResetMove();
    }

    void PlayAudioClip()
    {
        AudioManager.audioMan.PlaySource(roomID);
    }

    void ReduceTurnCount()
    {
        if (priest)
            turnCount--;

    }

    public void TriggerPriest()
    {
        priest = true;
    }

    public void EndTurn()
    {
        EndTurnEvents();
    }


    public void UpdatePoints(int _value)
    {
        ghostPower += _value;
        ShowPoints();

    }

    void GetPoint()
    {
        UpdatePoints(1);
    }

    private void ShowPoints()
    {
        CanvasController.canvas.AssignPower(ghostPower);

    }

    Room primary;

    public void MakeMove(Room _room)
    {
        if (changeLight)
        {
            _room.UpdateLigth(!_room.lightOn);
            changeLight = false;
        }
        else if (moveCount != 0)
        {
            if (_room.Verify(primary.name))
            {
                Move();
                primary = _room;
                primary.UpdatePlayerPosition(true);
                Debug.Log("ahora estas en " + primary.name);
            }
        }
    }

    void Move()
    {
        moveCount--;
    }


    public void SuperMove()
    {
        Debug.Log("Khi");
        moveCount = 2;
    }
    void ResetMove()
    {
        moveCount = 1;
    }
    public void LightState()
    {
        changeLight = true;
    }



}
