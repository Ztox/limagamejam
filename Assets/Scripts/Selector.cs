﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Selector : MonoBehaviour
{
    public GameObject tempConfirmation;
    [SerializeField] private int selectedPlayers;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SelectPlayers(int _number)
    {
        tempConfirmation.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = "Start game with " + _number + " players?";
        selectedPlayers = _number;
        tempConfirmation.SetActive(true);
    }

    public void StartGame()
    {
        foreach (Transform child in tempConfirmation.transform.GetChild(0).transform)
        {
            if (child.GetComponent<Button>())
            {
                child.GetComponent<Button>().interactable = false;
            }
        }

        if (selectedPlayers != 0)
            SceneManager.LoadScene(selectedPlayers);
    }

    public void CancelStart()
    {
        selectedPlayers = 0;
        tempConfirmation.SetActive(false);

    }
}
