﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{

    public static AudioManager audioMan;
    public AudioSource aSource;
    public AudioClip[] audioClips;
    public int temp;

    void Start()
    {
        if (!audioMan)
            audioMan = this;

        aSource = GetComponent<AudioSource>();
    }



    public void PlaySource(int _index)
    {
        switch (_index)
        {
            case 0:
                temp = Random.Range(0, 3);
                break;
            case 1:
                temp = Random.Range(2, 5);
                break;
            case 2:
                temp = Random.Range(5, 8);
                break;
            case 3:
                temp = Random.Range(7, 10);
                break;
            case 4:
                temp = 10;
                break;
            case 5:
                temp = Random.Range(10, 13);
                break;
            case 6:
                temp = 1;
                break;
            case 7:
                if (Random.value > .5) temp = 14;
                else temp = 5;
                break;
        }

        aSource.clip = audioClips[temp];
        aSource.Play();

        temp = -1;
    }
}
