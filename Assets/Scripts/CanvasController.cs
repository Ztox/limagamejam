﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasController : MonoBehaviour
{
    public GameManager gameManager;
    public GameObject powerPanel;
    public GameObject extrasPanel;
    private bool PowerPanel;
    private bool ExtrasPanel;

    public Text ghostPower;
    public Text turns;

    public static CanvasController canvas;

    // Start is called before the first frame update
    void Start()
    {
        if (!canvas)
            canvas = this;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void TogglePowerPanel()
    {
        if (!PowerPanel)
        {
            PowerPanel = true;
            ExtrasPanel = false;
        }
        else
        {
            PowerPanel = false;
        }
        PanelCheck();

    }
    public void ToggleExtrasPanel()
    {
        if (!ExtrasPanel)
        {
            ExtrasPanel = true;
            PowerPanel = false;
        }
        else
        {
            ExtrasPanel = false;
        }
        PanelCheck();
    }

    void PanelCheck()
    {
        if (PowerPanel)
            powerPanel.SetActive(true);
        else powerPanel.SetActive(false);

        if (ExtrasPanel)
            extrasPanel.SetActive(true);
        else extrasPanel.SetActive(false);

    }

    public void AssignPower(int newPower)
    {
        ghostPower.text = newPower.ToString();
    }

    public void AssingTurn(int newTurn)
    {
        turns.text = newTurn.ToString();
    }

    public void PowerCost(int _value)
    {
        if (gameManager.ghostPower >= _value)
        {
            gameManager.UpdatePoints(-_value);
            TogglePowerPanel();
        }
    }

    public void ChangeLightState()
    {
        gameManager.LightState();

        ToggleExtrasPanel();

    }

    public void SuperMove()
    {
        if (gameManager.ghostPower >= 2)
        {
            gameManager.SuperMove();
        }
    }
}
